import { defineStore } from 'pinia'

export const useColorStore = defineStore({
  id: 'color',
  state: () => ({
    colors: [
      '#b15578',
      '#5f2b4b',
      '#bda3b2',
      '#c07844',
      '#3a3359',
      '#b9b7b3'
    ]
  }),
  getters: {
    color: (state) => (i) => state.colors[i]
  },
  actions: {
    setColor (i, color) {
      this.colors[i] = color
    }
    // addColor () {
    //   this.colors.push(this.colors[this.colors.length - 1])
    // }
  }
})
