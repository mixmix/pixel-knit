import { defineStore } from 'pinia'
import { useColorStore } from './color'

export const usePatternStore = defineStore({
  id: 'pattern',
  state: () => ({
    rows: [
      new Array(12).fill(0),
      new Array(12).fill(0).map((_, i) => i % 4 === 0 ? 1 : 0),
      new Array(12).fill(0),
      new Array(12).fill(0).map((_, i) => i % 3 === 0 ? 1 : 0),
      new Array(12).fill(0)
    ],
    rowPalettes: [
      [0, 1],
      [0, 1],
      [2, 4],
      [0, 1],
      [0, 1]
    ],
    previewWidth: 70
  }),
  getters: {
    rowPalette: (state) => (i) => state.rowPalettes[i] || [0, 0],
    pixelColor (state) {
      return (row, col) => {
        const paletteIndex = state.rows[row][col]
        return this.paletteColor(row, paletteIndex)
      }
    },
    previewPixelColor (state) {
      return (row, col) => {
        // right-aligned modulo ...
        // - row            1 2 3 4   (width 4)
        // - preview    3 4 1 2 3 4   (width 6)
        // - remainder  3 4           (width 2)

        // TODO write a right-align-modulo fn + test
        const rowLength = state.rows[row].length
        const remainderLength = state.previewWidth % rowLength

        col = col < (remainderLength) // out by one?
          ? rowLength - remainderLength + col
          : (col - remainderLength) % rowLength

        return this.pixelColor(row, col)
      }
    },
    paletteColor: (state) => (row, paletteIndex) => {
      const color = useColorStore()

      const colorIndex = state.rowPalettes[row][paletteIndex]
      return color.color(colorIndex)
    }
  },
  actions: {
    addRow () {
      this.rows.unshift([...this.rows[0]]) // copy rows[last]
      this.rowPalettes.unshift([...this.rowPalettes[0]]) // copy rowPalettes[last]
    },
    removeRow () {
      this.rows.shift()
      this.rowPalettes.shift()
    },
    extendRow (row) {
      this.rows[row].unshift(0)
    },
    shortenRow (row) {
      this.rows[row].shift()
    },
    //
    // setRowColors
    cyclePixel (row, col) {
      this.rows[row][col] = (this.rows[row][col] + 1) % this.rowPalettes[row].length
    },
    cycleRowPalette (row, paletteIndex) {
      const color = useColorStore()
      this.rowPalettes[row][paletteIndex] = (this.rowPalettes[row][paletteIndex] + 1) % color.colors.length
    },
    setColor (i, color) {
      // ...
    }
  }
})
