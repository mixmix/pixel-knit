import test from 'tape'
import { setActivePinia, createPinia } from 'pinia'

import { useCounterStore } from './counter.js'

test('store/counter', t => {
  setActivePinia(createPinia())

  const counter = useCounterStore()

  t.equal(counter.counter, 0)
  t.equal(counter.doubleCount, 0)

  counter.increment()
  t.equal(counter.counter, 1)
  t.equal(counter.doubleCount, 2)

  t.end()
})
